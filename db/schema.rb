# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_21_035318) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string "name"
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_activities_on_event_id"
  end

  create_table "addresses", force: :cascade do |t|
    t.string "city"
    t.string "state"
    t.string "street"
    t.integer "cep"
    t.string "location_type"
    t.integer "location_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["location_type", "location_id"], name: "index_addresses_on_location_type_and_location_id"
  end

  create_table "certificates", force: :cascade do |t|
    t.bigint "user_id"
    t.string "certification_type"
    t.integer "certification_id"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["certification_type", "certification_id"], name: "index_certificates_on_certification_type_and_certification_id"
    t.index ["user_id"], name: "index_certificates_on_user_id"
  end

# Could not dump table "collaborators" because of following StandardError
#   Unknown type 'collaborator_kind' for column 'collaborator_kind'

  create_table "colleges", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.string "phone"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.bigint "college_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["college_id"], name: "index_courses_on_college_id"
  end

  create_table "departaments", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "edicts", force: :cascade do |t|
    t.string "name", limit: 50
    t.string "description", limit: 500
    t.bigint "departament_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["departament_id"], name: "index_edicts_on_departament_id"
  end

# Could not dump table "events" because of following StandardError
#   Unknown type 'status_event' for column 'status_event'

  create_table "kinds", force: :cascade do |t|
    t.string "name", limit: 50
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string "name", limit: 300
    t.string "description", limit: 500
    t.bigint "edict_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["edict_id"], name: "index_projects_on_edict_id"
  end

  create_table "records", force: :cascade do |t|
    t.datetime "entry"
    t.bigint "subscription_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subscription_id"], name: "index_records_on_subscription_id"
  end

# Could not dump table "subscriptions" because of following StandardError
#   Unknown type 'kind_subscription' for column 'kind_subscription'

# Could not dump table "team_members" because of following StandardError
#   Unknown type 'kind_sub' for column 'kind_sub'

  create_table "teams", force: :cascade do |t|
    t.string "teamgroup_type"
    t.integer "teamgroup_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["teamgroup_type", "teamgroup_id"], name: "index_teams_on_teamgroup_type_and_teamgroup_id"
  end

# Could not dump table "users" because of following StandardError
#   Unknown type 'kind' for column 'kind'

  add_foreign_key "activities", "events"
  add_foreign_key "certificates", "users"
  add_foreign_key "collaborators", "projects"
  add_foreign_key "collaborators", "users"
  add_foreign_key "contacts", "users"
  add_foreign_key "courses", "colleges"
  add_foreign_key "edicts", "departaments"
  add_foreign_key "events", "kinds"
  add_foreign_key "events", "projects"
  add_foreign_key "projects", "edicts"
  add_foreign_key "records", "subscriptions"
  add_foreign_key "subscriptions", "activities"
  add_foreign_key "subscriptions", "users"
  add_foreign_key "team_members", "teams"
  add_foreign_key "team_members", "users"
  add_foreign_key "users", "courses"
end

class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :city
      t.string :state
      t.string :street
      t.integer :cep
      t.string :location_type
      t.integer :location_id

      t.timestamps
    end
    add_index :addresses, [:location_type, :location_id]
  end
end

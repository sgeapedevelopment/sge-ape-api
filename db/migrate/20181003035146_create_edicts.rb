class CreateEdicts < ActiveRecord::Migration[5.2]
  def change
    create_table :edicts do |t|
      t.string :name, limit: 50
      t.string :description, limit: 500
      t.references :departament, foreign_key: true

      t.timestamps
    end
  end
end

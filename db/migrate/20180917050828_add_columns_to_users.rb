class AddColumnsToUsers < ActiveRecord::Migration[5.2]
  def change
    execute <<-SQL
      create type kind as enum ('user', 'subManager', 'manager', 'administrator');
      create type user_status as enum ('active', 'disable');
    SQL

    add_column :users, :name, :string, limit: 50, null: false
    add_column :users, :registration, :integer
    add_column :users, :date_nasc, :date
    add_index :users, :registration, unique: true
    add_column :users, :cpf, :string, null: false
    add_index :users, :cpf, unique: true
    add_column :users, :kind, :kind, index: true
    add_reference :users, :course, foreign_key: true
    add_column :users, :user_status, :user_status, index: true
  end
end

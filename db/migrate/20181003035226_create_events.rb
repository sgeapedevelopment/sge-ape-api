class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    execute <<-SQL
      create type status_event as enum ('open', 'closed');
    SQL

    create_table :events do |t|
      t.string :name, limit: 50, null: false
      t.references :kind, foreign_key: true
      t.references :project, foreign_key: true
      t.datetime :start_date
      t.datetime :end_date
      t.string :description, limit: 500

      t.timestamps
    end
    add_column :events, :status_event, :status_event, index: true
  end
end

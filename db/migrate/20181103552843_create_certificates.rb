class CreateCertificates < ActiveRecord::Migration[5.2]
  def change
    create_table :certificates do |t|
      t.references :user, foreign_key: true
      t.string :certification_type
      t.integer :certification_id
      t.string :description

      t.timestamps
    end
    add_index :certificates, [:certification_type, :certification_id]
  end
end

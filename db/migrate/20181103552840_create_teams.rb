class CreateTeams < ActiveRecord::Migration[5.2]
  def change
    create_table :teams do |t|
      t.string :teamgroup_type
      t.integer :teamgroup_id
      t.string :name

      t.timestamps
    end
    add_index :teams, [:teamgroup_type, :teamgroup_id]
  end
end

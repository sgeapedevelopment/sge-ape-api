class CreateTeamMembers < ActiveRecord::Migration[5.2]
  def change
    execute <<-SQL
      create type kind_sub as enum ('colaborou', 'coordenou', 'voluntario');
    SQL
    create_table :team_members do |t|
      t.references :user, foreign_key: true
      t.references :team, foreign_key: true

      t.timestamps
    end
    add_column :team_members, :kind_sub, :kind_sub, index: true
  end
end

class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :name, limit: 300
      t.string :description, limit: 500
      t.references :edict, foreign_key: true

      t.timestamps
    end
  end
end

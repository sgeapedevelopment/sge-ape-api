class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    execute <<-SQL
      create type kind_subscription as enum ('Participou', 'Ministrou', 'Coordenou');
    SQL

    create_table :subscriptions do |t|
      t.references :user, foreign_key: true
      t.references :activity, foreign_key: true

      t.timestamps
    end
    add_column :subscriptions, :kind_subscription, :kind_subscription, index: true
  end
end

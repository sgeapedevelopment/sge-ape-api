class CreateCollaborators < ActiveRecord::Migration[5.2]
  def change
    execute <<-SQL
      create type collaborator_kind as enum ('bolsista', 'voluntario');
    SQL

    create_table :collaborators do |t|
      t.references :user, foreign_key: true
      t.references :project, foreign_key: true
      t.string :workload

      t.timestamps
    end
    add_column :collaborators, :collaborator_kind, :collaborator_kind, index: true
  end
end

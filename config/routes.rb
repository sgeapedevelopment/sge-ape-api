require 'api_version_constraint'

Rails.application.routes.draw do
  devise_for :users, only: [:sessions], controllers: { sessions: 'ap1/v1/sessions' }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api, defaults: { format: :json }, path: '/' do
    namespace :v1, path: '/', constraints: ApiVersionConstraint.new(version: 1, default: true) do
      mount_devise_token_auth_for 'User', at: 'auth'
      resources :users, only: [:index, :show, :create, :update, :destroy]
      resources :colleges, only: [:index, :show, :create, :update, :destroy]
      resources :courses, only: [:index, :show, :create, :update, :destroy]
      resources :departaments, only: [:index, :show, :create, :update, :destroy]
      resources :events, only: [:index, :show, :create, :update, :destroy]
      resources :kinds, only: [:index, :show, :create, :update, :destroy]
      resources :subscriptions, only: [:index, :show, :create, :update, :destroy]
      resources :records, only: [:index, :show, :create, :update, :destroy]
      resources :edicts, only: [:index, :show, :create, :update, :destroy]
      resources :projects, only: [:index, :show, :create, :update, :destroy]
      resources :collaborators, only: [:index, :show, :create, :update, :destroy]
      resources :team_members, only: [:index, :show, :create, :update, :destroy]
      resources :activities, only: [:index, :show, :create, :update, :destroy]
      resources :sessions, only: [:create, :destroy]
    end
  end
end

class Api::V1::DepartamentSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :name, :links

  has_many :edicts

  def links
    [
      {
        rel: :self,
        href: api_v1_departament_path(object)
      }
    ]
  end
end

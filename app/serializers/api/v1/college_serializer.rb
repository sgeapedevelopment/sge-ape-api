class Api::V1::CollegeSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :name, :links

  has_many :courses

  def links
    [
      {
        rel: :self,
        href: api_v1_college_path(object)
      }
    ]
  end
end
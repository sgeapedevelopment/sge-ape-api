class Api::V1::ContactSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :phone, :links

  belongs_to :user

  def links
    [
      {
        rel: :self,
        href: api_v1_contact_path(object)
      }
    ]
  end
end

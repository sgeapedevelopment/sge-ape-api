class Api::V1::TeamMemberSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :kind_sub, :links

  belongs_to :user
  belongs_to :team

  def links
    [
      {
        rel: :self,
        href: api_v1_team_member_path(object)
      }
    ]
  end
end

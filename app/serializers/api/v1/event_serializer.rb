class Api::V1::EventSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :name, :kind, :project_id, :start_date, :end_date, :status_event, :links

  belongs_to :kind
  belongs_to :project
  has_one :address, as: :location
  has_many :activities
  has_many :teams, as: :teamgroup
  has_many :certificates, as: :certification

  def links
    [
      {
        rel: :self,
        href: api_v1_event_path(object)
      }
    ]
  end
end

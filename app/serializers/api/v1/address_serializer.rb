class Api::V1::AddressSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :city, :state, :street, :cep, :links

  belongs_to :location

  def links
    [
      {
        rel: :self,
        href: api_v1_address_path(object)
      }
    ]
  end
end

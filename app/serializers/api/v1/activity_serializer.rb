class Api::V1::ActivitySerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :name, :links

  belongs_to :event
  has_many :subscriptions
  has_many :teams, as: :teamgroup
  has_many :certificates, as: :certification

  def links
    [
      {
        rel: :self,
        href: api_v1_activity_path(object)
      }
    ]
  end
end

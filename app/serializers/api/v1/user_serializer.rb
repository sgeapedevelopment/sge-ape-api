class Api::V1::UserSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :cpf, :kind, :email, :auth_token, :registration, :name, :links
  type :user
  has_many :contact
  has_one :address

  def links
    [
      {
        rel: :self,
        href: api_v1_user_path(object)
      }
    ]
  end
end

class Api::V1::SubscriptionSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :kind_subscription, :links

  belongs_to :activity
  belongs_to :user
  has_many :records

  def links
    [
      {
        rel: :self,
        href: api_v1_subscription_path(object)
      }
    ]
  end
end

class Api::V1::TeamSerializer < ActiveModel::Serializer
  attributes :id, :name, :links

  belongs_to :teamgroup
  has_many :team_members

  def links
    [
      {
        rel: :self,
        href: api_v1_team_path(object)
      }
    ]
  end
end

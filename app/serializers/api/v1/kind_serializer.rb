class Api::V1::KindSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :name, :links

  has_many :events

  def links
    [
      {
        rel: :self,
        href: api_v1_kind_path(object)
      }
    ]
  end
end

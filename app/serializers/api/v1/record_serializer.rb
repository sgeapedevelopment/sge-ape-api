class Api::V1::RecordSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :entry, :links

  belongs_to :subscription

  def links
    [
      {
        rel: :self,
        href: api_v1_record_path(object)
      }
    ]
  end
end

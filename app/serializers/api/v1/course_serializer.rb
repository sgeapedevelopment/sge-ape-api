class Api::V1::CourseSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :name, :links

  belongs_to :college
  has_many :users

  def links
    [
      {
        rel: :self,
        href: api_v1_course_path(object)
      }
    ]
  end
end

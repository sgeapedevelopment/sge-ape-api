class Api::V1::CollaboratorSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :workload, :links

  belongs_to :user
  belongs_to :project

  def links
    [
      {
        rel: :self,
        href: api_v1_collaborator_path(object)
      }
    ]
  end
end

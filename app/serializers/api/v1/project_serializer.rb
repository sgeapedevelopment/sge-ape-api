class Api::V1::ProjectSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :name, :description, :links

  belongs_to :edict
  has_many :events
  has_many :collaborators
  has_many :certificates, as: :certification

  def links
    [
      {
        rel: :self,
        href: api_v1_project_path(object)
      }
    ]
  end
end

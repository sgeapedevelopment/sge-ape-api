class Api::V1::EdictSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :name, :description, :links

  belongs_to :departament
  has_many :projects

  def links
    [
      {
        rel: :self,
        href: api_v1_edict_path(object)
      }
    ]
  end
end

class Api::V1::SubscriptionsController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    @subscriptions = Subscription.order(id: :asc)
    render json: { subscriptions: @subscriptions }, status: 200
  end

  def new
    @subscription = Subscription.new
  end

  def show
    @subscription = Subscription.find(params[:id])
    respond_with @subscription, status: 200
  rescue StandardError
    head 404
  end

  def create
    @subscription = Subscription.new(subscription_params)

    if @subscription.save
      render json: @subscription, status: 201
    else
      # byebug
      render json: { errors: @subscription.errors }, status: 422
    end
  end

  def update
    @subscription = Subscription.find(params[:id])

    if @subscription.update(subscription_params)
      render json: @subscription, status: 200
    else
      render json: { errors: @subscription.errors }, status: 422
    end
  end

  def destroy
    @subscription = Subscription.find(params[:id])
    @subscription.destroy
    head 204
  end

  private

  def subscription_params
    params.require(:subscription).permit(:activity_id, :user_id, :kind_subscription,
                                         records_attributes: [:id, :subscription_id, :entry])
  end
end

class Api::V1::CollaboratorsController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    @collaborators = Collaborator.order(id: :asc)
    render json: { collaborators: @collaborators }, status: 200
  end

  def show
    @collaborator = Collaborator.find(params[:id])
    respond_with @collaborator
  rescue StandardError
    head 404
  end

  def create
    @collaborator = Collaborator.new(collaborator_params)

    if @collaborator.save
      render json: @collaborator, status: 201
    else
      render json: { errors: @collaborator.errors }, status: 422
    end
  end

  def update
    @collaborator = Collaborator.find(params[:id])

    if @collaborator.update(collaborator_params)
      render json: @collaborator, status: 200
    else
      render json: { errors: @collaborator.errors }, status: 422
    end
  end

  def destroy
    @collaborator = Collaborator.find(params[:id])
    @collaborator.destroy
    head 204
  end

  private

  def collaborator_params
    params.require(:collaborator).permit(:workload, :collaborator_kind, :user_id, :project_id)
  end
end

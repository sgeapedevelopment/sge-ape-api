class Api::V1::DepartamentsController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    @departaments = Departament.order(id: :asc)
    render json: { departaments: @departaments }, status: 200
  end

  def show
    @departament = Departament.find(params[:id])
    respond_with @departament
  rescue StandardError
    head 404
  end

  def create
    @departament = Departament.new(departament_params)
    if @departament.save
      render json: @departament, status: 201
    else
      render json: { errors: @departament.errors }, status: 422
    end
  end

  def update
    @departament = Departament.find(params[:id])

    if @departament.update(departament_params)
      render json: @departament, status: 200
    else
      render json: { errors: @departament.errors }, status: 422
    end
  end

  def destroy
    @departament = Departament.find(params[:id])
    @departament.destroy
    head 204
  end

  private

  def departament_params
    params.require(:departament).permit(:name)
  end
end

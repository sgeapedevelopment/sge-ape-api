class Api::V1::EventsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :edit, :update, :destory]
  respond_to :json

  def index
    @events = Event.order(id: :asc)
    render json: { events: @events }, status: 200
  end

  def new
    @event = Event.new
    @event.build_address
  end

  def show
    @event = Event.find(params[:id])
    respond_with @event, include: [:address, :teams], status: 200
  rescue StandardError
    head 404
  end

  def create
    @event = Event.new(event_params)

    if @event.save
      render json: @event, include: [:address, :teams], status: 201
    else
      # byebug
      render json: { errors: @event.errors }, status: 422
    end
  end

  def update
    @event = Event.find(params[:id])

    if @event.update(event_params)
      render json: @event, include: [:address, :teams], status: 200
    else
      render json: { errors: @event.errors }, status: 422
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    head 204
  end

  private

  def event_params
    params.require(:event).permit(
      :kind_id, :project_id, :start_date,
      :end_date, :name, :description, :status_event,
      address_attributes: [:id, :city, :state, :street, :cep],
      teams_attributes: [:id, :name]
    )
  end
end

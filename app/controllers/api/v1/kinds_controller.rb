class Api::V1::KindsController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    @kinds = Kind.order(id: :asc)
    render json: { kinds: @kinds }, status: 200
  end

  def show
    @kind = Kind.find(params[:id])
    respond_with @kind
  rescue StandardError
    head 404
  end

  def create
    @kind = Kind.new(kind_params)

    if @kind.save
      render json: @kind, status: 201
    else
      render json: { errors: @kind.errors }, status: 422
    end
  end

  def update
    @kind = Kind.find(params[:id])

    if @kind.update(kind_params)
      render json: @kind, status: 200
    else
      render json: { errors: @kind.errors }, status: 422
    end
  end

  def destroy
    @kind = Kind.find(params[:id])
    @kind.destroy
    head 204
  end

  private

  def kind_params
    params.require(:kind).permit(:name)
  end
end

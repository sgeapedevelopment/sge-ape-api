class Api::V1::TeamMembersController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    @teamMembers = TeamMember.order(id: :asc)
    render json: { teamMembers: @teamMembers }, status: 200
  end

  def new
    @teamMember = TeamMember.new
  end

  def show
    @teamMember = TeamMember.find(params[:id])
    respond_with @teamMember, status: 200
  rescue StandardError
    head 404
  end

  def create
    @teamMember = TeamMember.new(teamMember_params)

    if @teamMember.save
      render json: @teamMember, status: 201
    else
      # byebug
      render json: { errors: @teamMember.errors }, status: 422
    end
  end

  def update
    @teamMember = TeamMember.find(params[:id])

    if @teamMember.update(teamMember_params)
      render json: @teamMember, status: 200
    else
      render json: { errors: @teamMember.errors }, status: 422
    end
  end

  def destroy
    @teamMember = TeamMember.find(params[:id])
    @teamMember.destroy
    head 204
  end

  private

  def teamMember_params
    params.require(:team_member).permit(:user_id, :team_id, :kind_sub)
  end
end

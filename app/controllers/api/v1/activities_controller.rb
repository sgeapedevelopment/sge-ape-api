class Api::V1::ActivitiesController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    @activities = Activity.order(id: :asc)
    render json: { activities: @activities }, status: 200
  end

  def new
    @activity = Activity.new
    @activity.build_subscriptions
  end

  def show
    @activity = Activity.find(params[:id])
    respond_with @activity, status: 200
  rescue StandardError
    head 404
  end

  def create
    @activity = Activity.new(activity_params)

    if @activity.save
      render json: @activity, status: 201
    else
      # byebug
      render json: { errors: @activity.errors }, status: 422
    end
  end

  def update
    @activity = Activity.find(params[:id])

    if @activity.update(activity_params)
      render json: @activity, status: 200
    else
      render json: { errors: @activity.errors }, status: 422
    end
  end

  def destroy
    @activity = Activity.find(params[:id])
    @activity.destroy
    head 204
  end

  private

  def activity_params
    params.require(:activity).permit(:name, :event_id,
                                     subscriptions_attributes: [:id, :user_id, :activity_id, :kind_subscription],
                                     teams_attributes: [:id, :name])
  end
end

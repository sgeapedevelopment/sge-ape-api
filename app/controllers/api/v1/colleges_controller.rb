class Api::V1::CollegesController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    @colleges = College.order(id: :asc)
    render json: { colleges: @colleges }, status: 200
  end

  def show
    @college = College.find(params[:id])
    respond_with @college
  rescue StandardError
    head 404
  end

  def create
    @college = College.new(college_params)

    if @college.save
      render json: @college, status: 201
    else
      render json: { errors: @college.errors }, status: 422
    end
  end

  def update
    @college = College.find(params[:id])

    if @college.update(college_params)
      render json: @college, status: 200
    else
      render json: { errors: @college.errors }, status: 422
    end
  end

  def destroy
    @college = College.find(params[:id])
    @college.destroy
    head 204
  end

  private

  def college_params
    params.require(:college).permit(:name)
  end
end

class Api::V1::RecordsController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    @records = Record.order(id: :asc)
    render json: { records: @records }, status: 200
  end

  def show
    @record = Record.find(params[:id])
    respond_with @record
  rescue StandardError
    head 404
  end

  def create
    @record = Record.new(record_params)

    if @record.save
      render json: @record, status: 201
    else
      render json: { errors: @record.errors }, status: 422
    end
  end

  def update
    @record = Record.find(params[:id])

    if @record.update(record_params)
      render json: @record, status: 200
    else
      render json: { errors: @record.errors }, status: 422
    end
  end

  def destroy
    @record = Record.find(params[:id])
    @record.destroy
    head 204
  end

  private

  def record_params
    params.require(:record).permit(:subscription_id, :entry)
  end
end

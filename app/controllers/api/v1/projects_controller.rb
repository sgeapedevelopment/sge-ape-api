class Api::V1::ProjectsController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    @projects = Project.order(id: :asc)
    render json: { projects: @projects }, status: 200
  end

  def show
    @project = Project.find(params[:id])
    respond_with @project
  rescue StandardError
    head 404
  end

  def create
    @project = Project.new(project_params)

    if @project.save
      render json: @project, status: 201
    else
      render json: { errors: @project.errors }, status: 422
    end
  end

  def update
    @project = Project.find(params[:id])

    if @project.update(project_params)
      render json: @project, status: 200
    else
      render json: { errors: @project.errors }, status: 422
    end
  end

  def destroy
    @project = Project.find(params[:id])
    @project.destroy
    head 204
  end

  private

  def project_params
    params.require(:project).permit(:edict_id, :name, :description)
  end
end

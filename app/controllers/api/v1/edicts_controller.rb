class Api::V1::EdictsController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    @edicts = Edict.order(id: :asc)
    render json: { edicts: @edicts }, status: 200
  end

  def new
    @edict = Edict.new
  end

  def show
    @edict = Edict.find(params[:id])
    respond_with @edict, status: 200
  rescue StandardError
    head 404
  end

  def create
    @edict = Edict.new(edicts_params)

    if @edict.save
      render json: @edict, status: 201
    else
      # byebug
      render json: { errors: @edict.errors }, status: 422
    end
  end

  def update
    @edict = Edict.find(params[:id])

    if @edict.update(edicts_params)
      render json: @edict, status: 200
    else
      render json: { errors: @edict.errors }, status: 422
    end
  end

  def destroy
    @edict = Edict.find(params[:id])
    @edict.destroy
    head 204
  end

  private

  def edicts_params
    params.require(:edict).permit(:name, :departament_id, :description)
  end
end

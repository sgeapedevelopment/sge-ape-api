class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:course_id, :email, :kind,
      :password, :password_confirmation,
      :name, :cpf, :registration,
      :user_status, :date_nasc,
      address_attributes: [:id, :city, :state, :street, :cep],
      contact_attributes: [:id, :phone, :_destroy]])
  end
end

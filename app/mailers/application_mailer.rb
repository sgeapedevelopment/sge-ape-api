class ApplicationMailer < ActionMailer::Base
  default from: 'gsiqueira1996@gmail.com'
  layout 'mailer'
end

class College < ApplicationRecord
  has_many :courses

  validates_presence_of :name, on: :create, message: "can't be blank"
  validates_format_of :name, with: /[A-Z][a-z]/, message: 'is invalid'
end

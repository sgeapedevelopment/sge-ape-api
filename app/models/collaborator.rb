class Collaborator < ApplicationRecord
  belongs_to :user
  belongs_to :project

  enum collaborator_kind: {
    bolsista: 'bolsista',
    voluntario: 'voluntário(a)'
  }

  after_initialize :set_default_collaborator_kind, if: :new_record?

  def set_default_collaborator_kind
    self.collaborator_kind ||= :bolsista
  end

  validates_presence_of :workload, on: [:create, :update], message: "can't be blank"
end

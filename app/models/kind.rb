class Kind < ApplicationRecord
  has_many :events

  validates_presence_of :name, on: [:create, :update], message: "can't be blank"
end

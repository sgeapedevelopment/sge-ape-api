class Project < ApplicationRecord
  belongs_to :edict
  has_many :events, dependent: :destroy
  has_many :collaborators, dependent: :destroy
  has_many :certificates, as: :certification, dependent: :destroy

  validates_format_of :name, with: /[A-Z][a-z]/, on: [:create, :update], message: 'is invalid'
  validates_presence_of [:name, :description], on: [:create, :update], message: "can't be blank"
  validates_length_of :name, maximum: 50, allow_nil: false
  validates_length_of :description, maximum: 500, allow_nil: true
end

class Subscription < ApplicationRecord
  belongs_to :activity
  belongs_to :user
  has_many :records, dependent: :destroy

  enum kind_subscription: {
    Participou: 'Participou',
    Ministrou: 'Ministrou',
    Coordenou: 'Coordenou'
  }
  after_initialize :set_default_kind_subscription, if: :new_record?

  def set_default_kind_subscription
    self.kind_subscription ||= :Participou
  end

  accepts_nested_attributes_for :records, reject_if: :all_blank, allow_destroy: true

  validates_presence_of :kind_subscription, on: [:create, :upgrade], message: "can't be blank"
end

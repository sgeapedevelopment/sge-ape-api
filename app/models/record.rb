class Record < ApplicationRecord
  belongs_to :subscription

  validates_presence_of :entry, on: [:create, :update], message: "can't be blank"
end

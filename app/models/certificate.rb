class Certificate < ApplicationRecord
  belongs_to :user
  belongs_to :certification, polymorphic: true
end

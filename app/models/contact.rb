class Contact < ApplicationRecord
  belongs_to :user

  validates_presence_of :phone, on: [:create, :update], message: "can't be blank"
  validates_format_of :phone, with: /\d/, on: [:create, :update], message: 'is invalid'
end

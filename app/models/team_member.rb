class TeamMember < ApplicationRecord
  belongs_to :user
  belongs_to :team

  enum kind_sub: {
    colaborou: 'colaborou',
    coordenou: 'coordenou',
    voluntario: 'voluntario(a)'
  }
  after_initialize :set_default_kind_sub, if: :new_record?

  def set_default_kind_sub
    self.kind_sub ||= :colaborou
  end

  validates_presence_of :kind_sub, on: [:create, :update], message: "can't be blank"
end

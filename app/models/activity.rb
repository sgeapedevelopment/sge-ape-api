class Activity < ApplicationRecord
  belongs_to :event
  has_many :subscriptions, dependent: :destroy
  has_many :teams, as: :teamgroup, dependent: :destroy
  has_many :certificates, as: :certification, dependent: :destroy

  validates_presence_of :name, on: [:create, :update], message: "can't be blank"

  accepts_nested_attributes_for :subscriptions, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :teams, reject_if: :all_blank, allow_destroy: true
end

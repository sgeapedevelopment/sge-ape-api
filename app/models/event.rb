class Event < ApplicationRecord
  belongs_to :kind
  belongs_to :project
  has_one :address, as: :location, dependent: :destroy
  has_many :activities, dependent: :destroy
  has_many :teams, as: :teamgroup, dependent: :destroy
  has_many :certificates, as: :certification, dependent: :destroy

  enum status_event: {
    open: 'open',
    closed: 'closed'
  }
  after_initialize :set_default_status_event, if: :new_record?

  def set_default_status_event
    self.status_event ||= :open
  end

  accepts_nested_attributes_for :address, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :teams, reject_if: :all_blank, allow_destroy: true

  validates_presence_of :name, on: [:create, :update], message: "can't be blank"
end

class Team < ApplicationRecord
  belongs_to :teamgroup, polymorphic: true, optional: true
  has_many :team_members, dependent: :destroy

  validates_presence_of :name, on: [:create, :update], message: "can't be blank"
end

class Course < ApplicationRecord
  belongs_to :college
  has_many :users

  validates_presence_of :name, on: :create, message: "can't be blank"
  validates_format_of :name, with: /[A-Z][a-z]/, message: 'is invalid'
end

class User < ApplicationRecord
  require 'user_notification.rb'
  belongs_to :course
  has_one :address, as: :location, dependent: :destroy
  has_many :contact, dependent: :destroy
  has_many :subscriptions, dependent: :destroy
  has_many :team_members, dependent: :destroy
  has_many :collaborators, dependent: :destroy
  has_many :certificates, dependent: :destroy

  enum user_status: {
    active: 'active',
    disable: 'disable'
  }
  after_initialize :set_default_user_status, if: :new_record?

  def set_default_user_status
    self.user_status ||= :active
  end

  enum kind: {
    user: 'user',
    subManager: 'subManager',
    manager: 'manager',
    administrator: 'administrator'
  }
  after_initialize :set_default_kind, if: :new_record?

  after_create :send_welcome_notification

  def set_default_kind
    self.kind ||= :user
  end

  def send_welcome_notification
    @notifier = UserNotification.new
    @notifier.welcome(self)
  end
  

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :registerable, :recoverable, :rememberable, :validatable,
         :database_authenticatable, :authentication_keys => [:email, :cpf]

  include DeviseTokenAuth::Concerns::User

  accepts_nested_attributes_for :address, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :contact, reject_if: :all_blank, allow_destroy: true

  validates_presence_of :name, on: :create, message: "can't be blank"
  validates_presence_of :cpf, on: :create, message: "can't be blank"
  validates_cpf_format_of :cpf
  validates_uniqueness_of :cpf, on: :create, message: 'must be unique' 
end

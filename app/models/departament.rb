class Departament < ApplicationRecord
  has_many :edicts

  validates_format_of :name, with: /[A-Z][a-z]/, on: [:create, :update], message: 'is invalid'
  validates_presence_of :name, on: [:create, :update], message: "can't be blank"
  validates_length_of :name, maximum: 50, allow_nil: false
end

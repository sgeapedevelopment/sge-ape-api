class Address < ApplicationRecord
  belongs_to :location, polymorphic: true
  
  validates_presence_of :city, on: [:create, :update], message: "can't be blank"
end

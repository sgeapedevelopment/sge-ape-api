class UserNotification
  # using SendGrid's Ruby Library
  # https://github.com/sendgrid/sendgrid-ruby
  require 'sendgrid-ruby'
  require 'json'
  include SendGrid

  def welcome(user)
    @from = 'gsiqueira1996@gmail.com'
    @user = user
    mail = Mail.new
    mail.template_id = 'd-f1adba9940484edeb61458359830dfc1' 
    mail.from = Email.new(email: @from)
    mail.subject = 'Bem vindo ao SGE APE'
    personalization = Personalization.new
    personalization.add_to(Email.new(email: @user.email))
    mail.add_content(Content.new(type: 'text/plain', value: 'some text here'))
    mail.add_content(Content.new(type: 'text/html', value: '<html><body>some text here</body></html>'))
    mail.add_personalization(personalization)

    # puts JSON.pretty_generate(mail.to_json)
    puts mail.to_json

    sg = SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'])
    response = sg.client.mail._('send').post(request_body: mail.to_json)
    puts response.status_code
    puts response.body
    puts response.headers  
  end
end
require 'cpf_cnpj'

FactoryBot.define do
  factory :user do
    association(:course)
    email { Faker::Internet.email }
    password { '123456' }
    password_confirmation { password.to_s }
    name { Faker::Name.name_with_middle }
    cpf { CPF.generate }
    registration { Faker::Number.positive }
  end
  factory :contact do
    association(:user)
    phone { Faker::PhoneNumber.cell_phone }
  end
end

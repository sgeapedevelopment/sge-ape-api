FactoryBot.define do
  factory :edict do
    association(:departament)
    name { Faker::FunnyName.name }
    description { Faker::Lorem.paragraph(2) }
  end
end

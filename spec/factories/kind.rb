FactoryBot.define do
  factory :kind do
    name { Faker::FunnyName.name }
  end
end

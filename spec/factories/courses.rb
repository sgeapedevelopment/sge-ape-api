FactoryBot.define do
  factory :course do
    association(:college)
    name { Faker::FunnyName.two_word_name }
  end
end

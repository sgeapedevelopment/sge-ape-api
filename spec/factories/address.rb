FactoryBot.define do
  factory :address do
    city { teste }
    state { Faker::Address.state }
    street { Faker::Address.street_name }
    cep { Faker::Address.zip_code }

    trait :user do
      association :location, factory: :user
    end
    trait :event do
      association :location, factory: :event
    end
  end
end

FactoryBot.define do
  factory :project do
    association(:edict)
    name { Faker::FunnyName.name }
    description { Faker::Lorem.paragraph }
  end
end

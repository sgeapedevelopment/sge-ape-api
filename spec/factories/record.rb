FactoryBot.define do
  factory :record do
    association(:subscription)
    entry { '2018-10-05T12:55:00.000Z' }
  end
end

FactoryBot.define do
  factory :activity do
    association(:event)
    name { Faker::FunnyName.name }
  end
end

FactoryBot.define do
  factory :collaborator do
    association(:user)
    association(:project)
    workload { 4 }
  end
end

FactoryBot.define do
  factory :event do
    association(:kind)
    association(:project)
    name { Faker::FunnyName.name }
    start_date { Faker::Time.between(DateTime.now - 1, DateTime.now) }
    end_date { Faker::Time.between(DateTime.now - 1, DateTime.now) }
    description { Faker::HitchhikersGuideToTheGalaxy.marvin_quote }
  end
end

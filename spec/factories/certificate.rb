FactoryBot.define do
  factory :certificate do
    trait :activity do
      association :certification, factory: :activity
    end
    trait :event do
      association :certification, factory: :event
    end
    trait :project do
      association :certification, factory: :project
    end
  end
end

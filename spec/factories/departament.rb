FactoryBot.define do
  factory :departament do
    name { Faker::Commerce.department }
  end
end

FactoryBot.define do
  factory :team do
    name { Faker::FunnyName.name }

    trait :event do
      association :teamgroups, factory: :event
    end
    trait :activity do
      association :teamgroups, factory: :activity
    end
  end
end

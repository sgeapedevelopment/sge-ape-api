FactoryBot.define do
  factory :subscription do
    association(:activity)
    association(:user)
  end
end

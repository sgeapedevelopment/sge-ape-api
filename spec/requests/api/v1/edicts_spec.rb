require 'rails_helper'

RSpec.describe 'Edicts API', type: :request do
  let!(:edict) { create(:edict) }
  let(:edict_id) { edict.id }
  let(:departament) { create(:departament) }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  before { host! 'api.ape.test' }

  describe 'Get /edicts' do
    before do
      create_list(:edict, 9, departament_id: departament.id)
      get '/edicts', params: {}, headers: headers
    end

    it 'returns OK' do
      expect(response).to have_http_status(200)
    end

    it 'returns 10 edicts from database' do
      expect(json_response[:edicts].count).to eq(10)
    end
  end

  describe 'Get /edicts/id' do
    before do
      get "/edicts/#{edict_id}", params: {}, headers: headers
    end

    context 'when is found' do
      it 'description' do
        expect(json_response[:data][:id].to_i).to equal(edict_id)
      end

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when not found' do
      let(:edict_id) { 123_123 }

      it 'returns NOT FOUND' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'POST /edicts' do
    before do
      post '/edicts', params: { edict: edict_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:edict_params) { attributes_for(:edict, departament_id: departament.id) }

      it 'returns CREATED' do
        expect(response).to have_http_status(201)
      end

      it 'returns json data for the created edict' do
        expect(json_response[:data][:attributes][:name]).to eq(edict_params[:name])
      end
    end

    context 'when request not valid' do
      let(:edict_params) { attributes_for(:edict, departament_id: departament.id, name: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'PUT /edicts/id' do
    before do
      put "/edicts/#{edict_id}", params: { edict: edict_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:edict_params) { attributes_for(:edict,  departament_id: departament.id, name: 'Show de ROCK') }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the json data for the updated edict' do
        expect(json_response[:data][:attributes][:name]).to eq(edict_params[:name])
      end
    end

    context 'when request not valid' do
      let(:edict_params) { attributes_for(:edict, departament_id: departament.id, name: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the updated edict' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /edicts/:id' do
    before do
      delete "/edicts/#{edict_id}", params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(204)
    end

    it 'removes the edict from database' do
      expect(Edict.find_by(id: edict.id)).to be_nil
    end
  end
end

require 'rails_helper'

RSpec.describe 'team_members API', type: :request do
  let!(:team_member) { create(:team_member) }
  let(:team_member_id) { team_member.id }
  let(:team) { create(:team) }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  before { host! 'api.ape.test' }

  describe 'Get /team_members' do
    before do
      create_list(:team_member, 9, user_id: user.id, team_id: team.id)
      get '/team_members', params: {}, headers: headers
    end

    it 'returns OK' do
      expect(response).to have_http_status(200)
    end

    it 'returns 10 team_members from database' do
      expect(json_response[:teamMembers].count).to eq(10)
    end
  end

  describe 'GET /team_members/:id' do
    before do
      get "/team_members/#{team_member_id}", params: {}, headers: headers
    end

    context 'when is found' do
      it 'returns the team_member' do
        expect(json_response[:data][:id].to_i).to equal(team_member_id)
      end

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when not found' do
      let(:team_member_id) { 0o000000000000 }

      it 'returns NOT FOUND' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'POST /team_members/' do
    before do
      post '/team_members', params: { team_member: teamMember_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:teamMember_params) { attributes_for(:team_member, user_id: user.id, team_id: team.id) }

      it 'returns CREATED' do
        expect(response).to have_http_status(201)
      end

      it 'returns json data for the created team_member' do
        expect(json_response[:data][:attributes][:kind_sub]).to eq(teamMember_params[:kind_sub])
      end
    end

    context 'when request not valid' do
      let(:teamMember_params) { attributes_for(:team_member, user_id: user.id, team_id: 2_3, kind_sub: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'PUT /team_members/:id' do
    before do
      put "/team_members/#{team_member_id}", params: { team_member: teamMember_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:teamMember_params) { attributes_for(:team_member, user_id: user.id, team_id: team.id) }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the json data for the updated team_member' do
        expect(json_response[:data][:attributes][:kind_sub]).to eq(teamMember_params[:kind_sub])
      end
    end

    context 'when the request params are invalid' do
      let(:teamMember_params) { attributes_for(:team_member, user_id: user.id, team_id: 2_3, kind_sub: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the updated team_member' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /team_members/:id' do
    before do
      delete "/team_members/#{team_member_id}", params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(204)
    end

    it 'removes the team_members from database' do
      expect(TeamMember.find_by(id: team_member.id)).to be_nil
    end
  end
end

require 'rails_helper'

RSpec.describe 'Events API', type: :request do
  let!(:event) { create(:event) }
  let(:event_id) { event.id }
  let(:project) { create(:project) }
  let(:kind) { create(:kind) }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  before { host! 'api.ape.test' }

  describe 'Get /events' do
    before do
      create_list(:event, 9, kind_id: kind.id, project_id: project.id)
      get '/events', params: {}, headers: headers
    end

    it 'returns OK' do
      expect(response).to have_http_status(200)
    end

    it 'returns 10 events from database' do
      expect(json_response[:events].count).to eq(10)
    end
  end

  describe 'Get /events/id' do
    before do
      get "/events/#{event_id}", params: {}, headers: headers
    end

    context 'when is found' do
      it 'description' do
        expect(json_response[:data][:id].to_i).to equal(event_id)
      end

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when not found' do
      let(:event_id) { 123_123 }

      it 'returns NOT FOUND' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'POST /events' do
    before do
      post '/events', params: { event: event_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:event_params) { attributes_for(:event, kind_id: kind.id, project_id: project.id) }

      it 'returns CREATED' do
        expect(response).to have_http_status(201)
      end

      it 'returns json data for the created event' do
        expect(json_response[:data][:attributes][:name]).to eq(event_params[:name])
      end
    end

    context 'when request not valid' do
      let(:event_params) { attributes_for(:event, kind_id: kind.id, project_id: project.id, name: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'PUT /events/id' do
    before do
      put "/events/#{event_id}", params: { event: event_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:event_params) { attributes_for(:event, kind_id: kind.id, project_id: project.id, name: 'Show de ROCK') }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the json data for the updated event' do
        expect(json_response[:data][:attributes][:name]).to eq(event_params[:name])
      end
    end

    context 'when request not valid' do
      let(:event_params) { attributes_for(:event, kind_id: kind.id, project_id: project.id, name: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the updated event' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /events/:id' do
    before do
      delete "/events/#{event_id}", params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(204)
    end

    it 'removes the event from database' do
      expect(Event.find_by(id: event.id)).to be_nil
    end
  end
end

require 'rails_helper'

RSpec.describe 'kinds API', type: :request do
  let!(:kind) { create(:kind) }
  let(:kind_id) { kind.id }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  before { host! 'api.ape.test' }

  describe 'Get /kinds' do
    before do
      create_list(:kind, 9)
      get '/kinds', params: {}, headers: headers
    end

    it 'returns OK' do
      expect(response).to have_http_status(200)
    end

    it 'returns 10 kinds from database' do
      expect(json_response[:kinds].count).to eq(10)
    end
  end

  describe 'GET /kinds/:id' do
    before do
      get "/kinds/#{kind_id}", params: {}, headers: headers
    end

    context 'when is found' do
      it 'returns the kind' do
        expect(json_response[:data][:id].to_i).to equal(kind_id)
      end

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when not found' do
      let(:kind_id) { 0o000000000000 }

      it 'returns NOT FOUND' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'POST /kinds/' do
    before do
      post '/kinds', params: { kind: kind_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:kind_params) { attributes_for(:kind) }

      it 'returns CREATED' do
        expect(response).to have_http_status(201)
      end

      it 'returns json data for the created kind' do
        expect(json_response[:data][:attributes][:name]).to eq(kind_params[:name])
      end
    end

    context 'when request not valid' do
      let(:kind_params) { attributes_for(:kind, name: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'PUT /kinds/:id' do
    before do
      put "/kinds/#{kind_id}", params: { kind: kind_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:kind_params) { { name: 'Palestra' } }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the json data for the updated kind' do
        expect(json_response[:data][:attributes][:name]).to eq(kind_params[:name])
      end
    end

    context 'when the request params are invalid' do
      let(:kind_params) { attributes_for(:kind, name: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the updated kind' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /kinds/:id' do
    before do
      delete "/kinds/#{kind_id}", params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(204)
    end

    it 'removes the kinds from database' do
      expect(Kind.find_by(id: kind.id)).to be_nil
    end
  end
end

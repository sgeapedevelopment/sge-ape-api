require 'rails_helper'

RSpec.describe 'Projects API', type: :request do
  let!(:project) { create(:project) }
  let(:project_id) { project.id }
  let(:edict) { create(:edict) }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  before { host! 'api.ape.test' }

  describe 'Get /projects' do
    before do
      create_list(:project, 9, edict_id: edict.id)
      get '/projects', params: {}, headers: headers
    end

    it 'returns OK' do
      expect(response).to have_http_status(200)
    end

    it 'returns 10 projects from database' do
      expect(json_response[:projects].count).to eq(10)
    end
  end

  describe 'Get /projects/:id' do
    before do
      get "/projects/#{project_id}", params: {}, headers: headers
    end
    context 'when is found' do
      it 'returns the project' do
        expect(json_response[:data][:id].to_i).to equal(project_id)
      end

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when not found' do
      let(:project_id) { 123_333_333_123_123 }

      it 'returns NOT FOUND' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'POST /projects' do
    before do
      post '/projects', params: { project: project_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:project_params) { attributes_for(:project, edict_id: edict.id) }

      it 'has a valid factory' do
        expect(create(:project)).to be_valid
      end

      it 'returns CREATED' do
        expect(response).to have_http_status(201)
      end

      it 'returns json data for the created project' do
        expect(json_response[:data][:attributes][:name]).to eq(project_params[:name])
      end
    end

    context 'when request not valid' do
      let(:project_params) { attributes_for(:project, name: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'PUT /projects/:id' do
    before do
      put "/projects/#{project_id}", params: { project: project_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:project_params) { { name: 'Como Plantar Batata' } }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the json data for the updated project' do
        expect(json_response[:data][:attributes][:name]).to eq(project_params[:name])
      end
    end

    context 'when the request params are invalid' do
      let(:project_params) { { name: '____SD!@#!@#!@#!@#!@123123' } }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the updated project' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /projects/:id' do
    before do
      delete "/projects/#{project_id}", params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(204)
    end

    it 'removes the project from database' do
      expect(Project.find_by(id: project.id)).to be_nil
    end
  end
end

require 'rails_helper'

RSpec.describe 'Records API', type: :request do
  let!(:record) { create(:record) }
  let(:record_id) { record.id }
  let!(:subscription) { create(:subscription) }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  before { host! 'api.ape.test' }

  describe 'Get /records' do
    before do
      create_list(:record, 9, subscription_id: subscription.id)
      get '/records', params: {}, headers: headers
    end

    it 'returns OK' do
      expect(response).to have_http_status(200)
    end

    it 'returns 10 records from database' do
      expect(json_response[:records].count).to eq(10)
    end
  end

  describe 'GET /records/:id' do
    before do
      get "/records/#{record_id}", params: {}, headers: headers
    end

    context 'when is found' do
      it 'returns the record' do
        expect(json_response[:data][:id].to_i).to equal(record_id)
      end

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when not found' do
      let(:record_id) { 0o000000000000 }

      it 'returns NOT FOUND' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'POST /records/' do
    before do
      post '/records', params: { record: record_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:record_params) { attributes_for(:record, subscription_id: subscription.id) }

      it 'returns CREATED' do
        expect(response).to have_http_status(201)
      end

      it 'returns json data for the created record' do
        expect(json_response[:data][:attributes][:entry]).to eq(record_params[:entry])
      end
    end

    context 'when request not valid' do
      let(:record_params) { attributes_for(:record, entry: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'PUT /records/:id' do
    before do
      put "/records/#{record_id}", params: { record: record_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:record_params) { { entry: '2018-10-05T12:55:00.000Z' } }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the json data for the updated record' do
        expect(json_response[:data][:attributes][:entry]).to eq(record_params[:entry])
      end
    end

    context 'when the request params are invalid' do
      let(:record_params) { { entry: '' } }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the updated record' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /records/:id' do
    before do
      delete "/records/#{record_id}", params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(204)
    end

    it 'removes the records from database' do
      expect(Record.find_by(id: record.id)).to be_nil
    end
  end
end

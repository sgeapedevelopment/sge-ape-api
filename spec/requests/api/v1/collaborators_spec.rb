require 'rails_helper'

RSpec.describe 'Collaborators API', type: :request do
  let!(:collaborator) { create(:collaborator) }
  let(:collaborator_id) { collaborator.id }
  let(:project) { create(:project) }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  before { host! 'api.ape.test' }

  describe 'Get /collaborators' do
    before do
      create_list(:collaborator, 9, user_id: user.id, project_id: project.id)
      get '/collaborators', params: {}, headers: headers
    end

    it 'returns OK' do
      expect(response).to have_http_status(200)
    end

    it 'returns 10 collaborators from database' do
      expect(json_response[:collaborators].count).to eq(10)
    end
  end

  describe 'Get /collaborator/:id' do
    before do
      get "/collaborators/#{collaborator_id}", params: {}, headers: headers
    end

    context 'when is found' do
      it 'returns the collaborator' do
        expect(json_response[:data][:id].to_i).to equal(collaborator_id)
      end

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when not found' do
      let(:collaborator_id) { 123_333_333_123_123 }

      it 'returns NOT FOUND' do
        expect(response).to  have_http_status(404)
      end
    end
  end

  describe 'POST /collaborators' do
    before do
      post '/collaborators', params: { collaborator: collaborator_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:collaborator_params) { attributes_for(:collaborator, user_id: user.id, project_id: project.id) }

      it 'returns CREATED' do
        expect(response).to  have_http_status(201)
      end

      it 'returns json data for the created collaborator' do
        expect(json_response[:data][:attributes][:workload].to_i).to eq(collaborator_params[:workload])
      end
    end

    context 'when request not valid' do
      let(:collaborator_params) { attributes_for(:collaborator, workload: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'PUT /collaborator/:id' do
    before do
      put "/collaborators/#{collaborator_id}", params: { collaborator: collaborator_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:collaborator_params) { { workload: '5' } }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the json data for the updated collaborator' do
        expect(json_response[:data][:attributes][:workload]).to eq(collaborator_params[:workload])
      end
    end

    context 'when request not valid' do
      let(:collaborator_params) { { workload: '' } }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the updated collaborator' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /collaborator/:id' do
    before do
      delete "/collaborators/#{collaborator_id}", params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(204)
    end

    it 'removes the collaborator from database' do
      expect(Collaborator.find_by(id: collaborator.id)).to be_nil
    end
  end
end

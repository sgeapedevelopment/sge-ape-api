require 'rails_helper'

RSpec.describe 'subscriptions API', type: :request do
  let!(:subscription) { create(:subscription) }
  let(:subscription_id) { subscription.id }
  let(:activity) { create(:activity) }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  before { host! 'api.ape.test' }

  describe 'Get /subscriptions' do
    before do
      create_list(:subscription, 9, user_id: user.id, activity_id: activity.id)
      get '/subscriptions', params: {}, headers: headers
    end

    it 'returns OK' do
      expect(response).to have_http_status(200)
    end

    it 'returns 10 subscriptions from database' do
      expect(json_response[:subscriptions].count).to eq(10)
    end
  end

  describe 'GET /subscriptions/:id' do
    before do
      get "/subscriptions/#{subscription_id}", params: {}, headers: headers
    end

    context 'when is found' do
      it 'returns the subscription' do
        expect(json_response[:data][:id].to_i).to equal(subscription_id)
      end

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when not found' do
      let(:subscription_id) { 0o000000000000 }

      it 'returns NOT FOUND' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'POST /subscriptions/' do
    before do
      post '/subscriptions', params: { subscription: subscription_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:subscription_params) { attributes_for(:subscription, user_id: user.id, activity_id: activity.id) }

      it 'returns CREATED' do
        expect(response).to have_http_status(201)
      end

      it 'returns json data for the created subscription' do
        expect(json_response[:data][:attributes][:kind_subscription]).to eq(subscription_params[:kind_subscription])
      end
    end

    context 'when request not valid' do
      let(:subscription_params) { attributes_for(:subscription, user_id: user.id, activity_id: 2_3, kind_subscription: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'PUT /subscriptions/:id' do
    before do
      put "/subscriptions/#{subscription_id}", params: { subscription: subscription_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:subscription_params) { attributes_for(:subscription, user_id: user.id, activity_id: activity.id) }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the json data for the updated subscription' do
        expect(json_response[:data][:attributes][:kind_subscription]).to eq(subscription_params[:kind_subscription])
      end
    end

    context 'when the request params are invalid' do
      let(:subscription_params) { attributes_for(:subscription, user_id: user.id, activity_id: 2_3, kind_subscription: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the updated subscription' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /subscriptions/:id' do
    before do
      delete "/subscriptions/#{subscription_id}", params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(204)
    end

    it 'removes the subscriptions from database' do
      expect(Subscription.find_by(id: subscription.id)).to be_nil
    end
  end
end

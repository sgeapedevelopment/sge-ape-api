require 'rails_helper'

RSpec.describe 'Courses API', type: :request do
  let!(:course) { create(:course) }
  let!(:college) { create(:college) }
  let(:course_id) { course.id }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  before { host! 'api.ape.test' }

  describe 'Get /courses' do
    before do
      create_list(:course, 8, college_id: college.id)
      get '/courses', params: {}, headers: headers
    end

    it 'returns OK' do
      expect(response).to have_http_status(200)
    end

    it 'returns 10 courses from database' do
      expect(json_response[:courses].count).to eq(10)
    end
  end

  describe 'Get /courses/:id' do
    before do
      get "/courses/#{course_id}", params: {}, headers: headers
    end

    context 'when is found' do
      it 'returns the course' do
        expect(
          json_response[:data][:id].to_i).to equal(course_id)
      end

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when not found' do
      let(:course_id) { 123_333_333_123_123 }

      it 'returns NOT FOUND' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'POST /courses' do
    before do
      post '/courses', params: { course: course_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:course_params) { attributes_for(:course, college_id: college.id) }

      it 'has a valid factory' do
        expect(create(:course)).to be_valid
      end

      it 'returns CREATED' do
        expect(response).to have_http_status(201)
      end

      it 'returns json data for the created course' do
        expect(json_response[:data][:attributes][:name]).to eq(course_params[:name])
      end
    end

    context 'when request not valid' do
      let(:course_params) { attributes_for(:course, name: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'PUT /courses/:id' do
    before do
      put "/courses/#{course_id}", params: { course: course_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:course_params) { { name: 'Como Plantar Batata' } }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the json data for the updated course' do
        expect(json_response[:data][:attributes][:name]).to eq(course_params[:name])
      end
    end

    context 'when request not valid' do
      let(:course_params) { { name: '' } }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the updated course' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /courses/:id' do
    before do
      delete "/courses/#{course_id}", params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(204)
    end

    it 'removes the course from database' do
      expect(Course.find_by(id: course.id)).to be_nil
    end
  end
end

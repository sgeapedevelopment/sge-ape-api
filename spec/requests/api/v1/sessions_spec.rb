require 'rails_helper'

RSpec.describe 'Sessions API', type: :request do
  before { host! 'api.ape.test' }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  describe 'POST /auth/sign_in' do
    before do
      post '/auth/sign_in', params: credentials.to_json, headers: headers
    end

    context 'when are valid' do
      let(:credentials) { { email: user.email, password: '123456' } }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the authentication data in the headers' do
        expect(response.headers).to have_key('access-token')
        expect(response.headers).to have_key('uid')
        expect(response.headers).to have_key('client')
      end
    end

    context 'when are invalid' do
      let(:credentials) { { email: user.email, password: 'invalid_password' } }

      it 'returns Unauthorized' do
        expect(response).to have_http_status(401)
      end

      it 'returns the json data for errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /auth/sign_out' do
    let(:auth_token) { user.auth_token }

    before do
      delete '/auth/sign_out', params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(200)
    end

    it 'changes the user auth token' do
      user.reload
      expect( user.valid_token?(auth_data['access-token'], auth_data['client']) ).to eq(false)
    end
  end
end

require 'rails_helper'

RSpec.describe 'Activities API', type: :request do
  let!(:activity) { create(:activity) }
  let(:activity_id) { activity.id }
  let(:event) { create(:event) }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  before { host! 'api.ape.test' }

  describe 'Get /activities' do
    before do
      create_list(:activity, 9, event_id: event.id)
      get '/activities', params: {}, headers: headers
    end

    it 'returns OK' do
      expect(response).to have_http_status(200)
    end

    it 'returns 10 activities from database' do
      expect(json_response[:activities].count).to eq(10)
    end
  end

  describe 'Get /activities/:id' do
    before do
      get "/activities/#{activity_id}", params: {}, headers: headers
    end

    context 'when is found' do
      it 'returns the activity' do
        expect(json_response[:data][:id].to_i).to equal(activity_id)
      end

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when not found' do
      let(:activity_id) { 123_333_333_123_123 }

      it 'returns NOT FOUND' do
        expect(response).to  have_http_status(404)
      end
    end
  end

  describe 'POST /activities' do
    before do
      post '/activities', params: { activity: activity_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:activity_params) { attributes_for(:activity, event_id: event.id) }

      it 'returns CREATED' do
        expect(response).to  have_http_status(201)
      end

      it 'returns json data for the created activity' do
        expect(json_response[:data][:attributes][:name]).to eq(activity_params[:name])
      end
    end

    context 'when request not valid' do
      let(:activity_params) { attributes_for(:activity, name: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'PUT /activity/:id' do
    before do
      put "/activities/#{activity_id}", params: { activity: activity_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:activity_params) { { name: 'teste' } }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the json data for the updated activity' do
        expect(json_response[:data][:attributes][:name]).to eq(activity_params[:name])
      end
    end

    context 'when request not valid' do
      let(:activity_params) { { name: '' } }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the updated activity' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /activity/:id' do
    before do
      delete "/activities/#{activity_id}", params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(204)
    end

    it 'removes the activity from database' do
      expect(Activity.find_by(id: activity.id)).to be_nil
    end
  end
end

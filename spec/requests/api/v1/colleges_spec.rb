require 'rails_helper'

RSpec.describe 'Colleges API', type: :request do
  let!(:college) { create(:college) }
  let(:college_id) { college.id }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  before { host! 'api.ape.test' }

  describe 'Get /colleges' do
    before do
      create_list(:college, 8)
      get '/colleges', params: {}, headers: headers
    end

    it 'returns OK' do
      expect(response).to have_http_status(200)
    end

    it 'returns 10 colleges from database' do
      expect(json_response[:colleges].count).to eq(10)
    end
  end

  describe 'GET /colleges/:id' do
    before do
      get "/colleges/#{college_id}", params: {}, headers: headers
    end

    context 'when is found' do
      it 'returns the college' do
        expect(json_response[:data][:id].to_i).to equal(college_id)
      end

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when not found' do
      let(:college_id) { 0o000000000000 }

      it 'returns NOT FOUND' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'POST /colleges/' do
    before do
      post '/colleges', params: { college: college_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:college_params) { attributes_for(:college) }

      it 'returns CREATED' do
        expect(response).to have_http_status(201)
      end

      it 'returns json data for the created college' do
        expect(json_response[:data][:attributes][:name]).to eq(college_params[:name])
      end
    end

    context 'when request not valid' do
      let(:college_params) { attributes_for(:college, name: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'PUT /colleges/:id' do
    before do
      put "/colleges/#{college_id}", params: { college: college_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:college_params) { { name: 'Dança no fogo' } }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the json data for the updated college' do
        expect(json_response[:data][:attributes][:name]).to eq(college_params[:name])
      end
    end

    context 'when the request params are invalid' do
      let(:college_params) { { name: '' } }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the updated college' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /colleges/:id' do
    before do
      delete "/colleges/#{college_id}", params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(204)
    end

    it 'removes the colleges from database' do
      expect(College.find_by(id: college.id)).to be_nil
    end
  end
end

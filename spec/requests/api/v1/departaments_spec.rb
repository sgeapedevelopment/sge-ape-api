require 'rails_helper'

RSpec.describe 'Departaments API', type: :request do
  let!(:departament) { create(:departament) }
  let(:departament_id) { departament.id }
  let!(:user) { create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.ape.v1',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'uid' => auth_data['uid'],
      'client' => auth_data['client']
    }
  end

  before { host! 'api.ape.test' }

  describe 'Get /departaments' do
    before do
      create_list(:departament, 9)
      get '/departaments', params: {}, headers: headers
    end

    it 'returns OK' do
      expect(response).to have_http_status(200)
    end

    it 'returns 10 departaments from database' do
      expect(json_response[:departaments].count).to eq(10)
    end
  end

  describe 'Get /departaments/:id' do
    before do
      get "/departaments/#{departament_id}", params: {}, headers: headers
    end

    context 'when is found' do
      it 'returns the departament' do
        expect(
          json_response[:data][:id].to_i).to equal(departament_id)
      end

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when not found' do
      let(:departament_id) { 123_333_333_123_123 }

      it 'returns NOT FOUND' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'POST /departaments' do
    before do
      post '/departaments', params: { departament: departament_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:departament_params) { attributes_for(:departament) }

      it 'has a valid factory' do
        expect(create(:departament)).to be_valid
      end

      it 'returns CREATED' do
        expect(response).to have_http_status(201)
      end

      it 'returns json data for the created departament' do
        expect(json_response[:data][:attributes][:name]).to eq(departament_params[:name])
      end
    end

    context 'when request not valid' do
      let(:departament_params) { attributes_for(:departament, name: '') }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'PUT /departaments/:id' do
    before do
      put "/departaments/#{departament_id}", params: { departament: departament_params }.to_json, headers: headers
    end

    context 'when request are valid' do
      let(:departament_params) { { name: 'Como Plantar Batata' } }

      it 'returns OK' do
        expect(response).to have_http_status(200)
      end

      it 'returns the json data for the updated departament' do
        expect(json_response[:data][:attributes][:name]).to eq(departament_params[:name])
      end
    end

    context 'when the request params are invalid' do
      let(:departament_params) { { name: '____SD!@#!@#!@#!@#!@123123' } }

      it 'returns Unprocessable Entity' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the updated departament' do
        expect(json_response).to have_key(:errors)
      end
    end
  end

  describe 'DELETE /departaments/:id' do
    before do
      delete "/departaments/#{departament_id}", params: {}, headers: headers
    end

    it 'returns No Content' do
      expect(response).to have_http_status(204)
    end

    it 'removes the departament from database' do
      expect(Departament.find_by(id: departament.id)).to be_nil
    end
  end
end
  
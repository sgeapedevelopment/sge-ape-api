require 'rails_helper'

RSpec.describe Project, type: :model do
  let(:project) { build(:project) }

  before { project.name = '' }
  it { is_expected.not_to allow_value('').for(:name) }
  it { is_expected.not_to allow_value('@asds123124').for(:name) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to belong_to(:edict) }
  it { is_expected.to have_many(:events) }
  it { is_expected.to validate_length_of(:description).is_at_most(500) }
  it { is_expected.to have_many(:collaborators) }
  it { is_expected.to validate_length_of(:name).is_at_most(50) }
end

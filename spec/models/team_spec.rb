require 'rails_helper'

RSpec.describe Team, type: :model do
  let(:team) { build(:team) }

  before { team.name = '' }
  it { is_expected.to validate_presence_of(:name) }
end

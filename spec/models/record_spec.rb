require 'rails_helper'

RSpec.describe Record, type: :model do
  let(:record) { build(:record) }

  before { record.entry = '' }
  it { is_expected.to validate_presence_of(:entry) }
  it { is_expected.to validate_presence_of(:entry) }
end

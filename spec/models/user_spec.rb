require 'rails_helper'

RSpec.describe User, type: :model do
  let!(:user) { create(:user) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to have_one(:address) }
  it { is_expected.to have_many(:contact) }
  it { is_expected.to validate_uniqueness_of(:auth_token) }

  describe '#info' do
    it 'returns name and created_at' do
      allow(Devise).to receive(:friendly_token).and_return('asdasd12313TOKEN')
      expect(user.info).to eq("#{user.name} - #{user.created_at} - Token: #{Devise.friendly_token}")
    end
  end  

  describe '#generate_authentication_token!' do
    it 'generates a unique auth token' do
      allow(Devise).to receive(:friendly_token).and_return('asdasd12313TOKEN')
      user.generate_authentication_token!

      expect(user.auth_token).to eq('asdasd12313TOKEN')
    end

    it 'generates another auth token when the current auth token already has been taken' do
      allow(Devise).to receive(:friendly_token).and_return('avasdassssTONEN12312312313', 'avasdassssTONEN12312312313', 'acsdsddsa23123SADscs') 
      existing_user = create(:user, auth_token: 'avasdassssTONEN12312312313')
      user.generate_authentication_token!

      expect(user.auth_token).not_to eq(existing_user.auth_token)
    end
  end
end

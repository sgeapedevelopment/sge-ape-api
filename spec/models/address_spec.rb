require 'rails_helper'

RSpec.describe Address, type: :model do
  let(:address) { build(:address) }
  it { is_expected.to validate_presence_of(:city) }
end

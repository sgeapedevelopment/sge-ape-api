require 'rails_helper'

RSpec.describe Collaborator, type: :model do
  let(:collaborator) { build(:collaborator) }

  
  it { is_expected.to validate_presence_of(:workload) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:project) }
  
end

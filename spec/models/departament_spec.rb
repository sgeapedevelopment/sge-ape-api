require 'rails_helper'

RSpec.describe Departament, type: :model do
  let(:departament) { build(:departament) }

  before { departament.name = '' }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to allow_value('Extensao').for(:name) }
  it { is_expected.not_to allow_value('@asds123124').for(:name) }
  it { is_expected.to validate_length_of(:name).is_at_most(50) }
  it { is_expected.to have_many(:edicts) }
end

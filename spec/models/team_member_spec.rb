require 'rails_helper'

RSpec.describe TeamMember, type: :model do
  let(:team_member) { build(:team_member) }

  it { is_expected.to validate_presence_of(:kind_sub) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:team) }
end

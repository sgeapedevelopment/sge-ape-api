require 'rails_helper'

RSpec.describe Edict, type: :model do
  let(:edict) { build(:edict) }

  before { edict.name = '' }
  
  it { is_expected.not_to allow_value('').for(:name) }
  it { is_expected.not_to allow_value('@asds123124').for(:name) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_length_of(:name).is_at_most(50) }
  it { is_expected.to allow_value('Extensao').for(:name) }
  it { is_expected.to belong_to(:departament) }
end

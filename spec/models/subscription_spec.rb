require 'rails_helper'

RSpec.describe Subscription, type: :model do
  let(:subscription) { build(:subscription) }

  before { subscription.kind_subscription = '' }
  it { is_expected.to validate_presence_of(:kind_subscription) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:activity) }
end

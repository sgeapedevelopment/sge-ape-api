require 'rails_helper'

RSpec.describe College, type: :model do
  let(:college) { build(:college) }

  before { college.name = '' }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to have_many(:courses) }
end

require 'rails_helper'

RSpec.describe Activity, type: :model do
  let(:activity) { build(:activity) }

  before { activity.name = '' }

  it { is_expected.to validate_presence_of(:name) }
end

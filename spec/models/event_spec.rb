require 'rails_helper'

RSpec.describe Event, type: :model do
  let(:event) { build(:event) }

  before { event.name = '' }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:name) }
end

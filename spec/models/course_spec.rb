require 'rails_helper'

RSpec.describe Course, type: :model do
  let(:course) { build(:course) }

  before { course.name = '' }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to have_many(:users) }
end

require 'rails_helper'

RSpec.describe Contact, type: :model do
  let(:contact) { build(:contact) }
  let(:user) { build(:user) }

  before { contact.user_id = user.id }

  it { is_expected.to validate_presence_of(:phone) }
end

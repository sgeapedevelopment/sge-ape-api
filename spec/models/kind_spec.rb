require 'rails_helper'

RSpec.describe Kind, type: :model do
  let(:kind) { build(:kind) }

  before { kind.name = '' }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:name) }
end
